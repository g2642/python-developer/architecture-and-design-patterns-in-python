from datetime import date

from views import Index, Lesson1, Lesson2, Lesson3


# front controller
def secret_front(request):
    request['data'] = date.today()


def other_front(request):
    request['key'] = 'key'


fronts = [secret_front, other_front]

routes = {
    '/': Index(),
    '/lessons/1/': Lesson1(),
    '/lessons/2/': Lesson2(),
    '/lessons/3/': Lesson3(),
}
