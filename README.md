# My Framework

## Installing
```
1. python -m venv venv
2. source venv/bin/activate
3. pip install -r requirements.txt
```

## Running

```
python run.py [port]
```


