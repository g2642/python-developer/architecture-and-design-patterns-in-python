from my_framework.templator import render


class Index:
    def __call__(self, request):
        return '200 OK', render('index.html', data=request.get('data', None))


class Lesson1:
    def __call__(self, request):
        return '200 OK', render('lesson1.html', data=request.get('data', None))


class Lesson2:
    def __call__(self, request):
        return '200 OK', render('lesson2.html', data=request.get('data', None))


class Lesson3:
    def __call__(self, request):
        return '200 OK', render('lesson3.html', data=request.get('data', None))


class NotFound404:
    def __call__(self, request):
        return '404 WHAT', '404 PAGE Not Found'
