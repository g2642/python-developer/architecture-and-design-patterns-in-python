import sys
from wsgiref.simple_server import make_server

from my_framework.main import Framework
from urls import routes, fronts

application = Framework(routes, fronts)

args = sys.argv
if len(args) == 2 and args[-1].isdigit():
    PORT = int(sys.argv[-1])
else:
    PORT = 8080
with make_server('', PORT, application) as httpd:
    print(f"Server started on port: {PORT}")
    print(f"http://localhost:{PORT}")
    httpd.serve_forever()
